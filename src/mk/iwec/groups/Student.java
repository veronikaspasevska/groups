package mk.iwec.groups;

public class Student {
	private Integer id;
	private String name;
	private String last_name;

	public Student() {
		super();
	}

	public Student(Integer id, String name, String last_name) {
		super();
		this.id = id;
		this.name = name;
		this.last_name = last_name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	@Override
	public String toString() {
		return " " + " - " + name + " " + last_name;
	}

}
