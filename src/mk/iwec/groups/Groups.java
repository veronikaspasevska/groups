package mk.iwec.groups;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Groups {

	private List<Student> students;

	public Groups() {
		students = new ArrayList<>();
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public void readFile(String fileName) {
		try (BufferedReader in = new BufferedReader(new FileReader(fileName))) {
			String line;
			in.readLine();
			while ((line = in.readLine()) != null) {
				String[] lineContent = line.split(",");

				int id = Integer.valueOf(lineContent[0]);
				String name = lineContent[1];
				String lastName = lineContent[2];
				Student s = new Student(id, name, lastName);

				students.add(s);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void creatingTwoGroups() {
		Collections.shuffle(students);
		for (int i = 1; i <= 2; i++) {
			System.out.println("The members of group " + i + " are:");
			for (int j = 2; j >= 0; j--) {

				System.out.println(students.get(j));
				students.remove(j);

			}

		}
		creatingThirdGroup();

	}

	public void creatingThirdGroup() {
		System.out.println("The members of group 3 are: ");
		for (Student el : students) {
			System.out.println(el);

		}

	}
}
